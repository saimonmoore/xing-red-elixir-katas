defmodule Fibonacci do
  @moduledoc """
  Documentation for Fibonacci.
  """

  @doc """
  Calculates Fibonacci

  ## Examples

      iex> Fibonacci.call(5)
      5

  """
  def call(n) do
    n
  end
end
