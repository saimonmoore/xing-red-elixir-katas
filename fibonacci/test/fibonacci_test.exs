defmodule FibonacciTest do
  use ExUnit.Case
  doctest Fibonacci

  test "Fibonacci for 10" do
    assert Fibonacci.call(10) == 55
  end

  test "Fibonacci for 13" do
    assert Fibonacci.call(13) == 233
  end

  test "fibonacci for 26" do
    assert Fibonacci.call(26) == 121_393
  end

  test "fibonacci for 39" do
    assert Fibonacci.call(39) == 63_245_986
  end

  test "fibonacci for 60" do
    assert Fibonacci.call(60) == 1_548_008_755_920
  end
end
