# Swapi
Surprise! It's time now to start something from scratch.

Please don't use the existing Elixir client for Swapi. The idea is to implement your own.

## Tasks
* Create a Phoenix application
* Provide the following endpoints:
  * `GET /people`: lists the people from https://swapi.co/documentation#people using pagination
  * `GET /characters`: lists the people from https://swapi.co/documentation#people using pagination and populating all the information for the films the character appears on (director, producer...) instead of the url for the file resource.
  * `GET /vehicles`: lists **all** the vehicles from https://swapi.co/documentation#vehicles in one response
